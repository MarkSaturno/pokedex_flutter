import 'package:flutter/material.dart';
import 'package:pokedex/utilities/app_router.dart';

import 'feature/homescreen/home_page_screen.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Pokedex',
      initialRoute: HomePage.route,
      onGenerateRoute: AppRouter.generateRoute,
    );
  }
}
