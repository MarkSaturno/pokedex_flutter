import 'package:flutter/material.dart';

class DetailPage extends StatelessWidget {
  static const String route = '/detail';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Name of the pokemon'),
      ),
      body: SizedBox(
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: const [
            Expanded(
              flex: 3,
              child: Text('picture'),
            ),
            Expanded(
              flex: 3,
              child: Text('stats'),
            ),
          ],
        ),
      ),
    );
  }
}
