import 'package:flutter/material.dart';
import 'package:pokedex/feature/details/detail_page_screen.dart';

class HomePage extends StatelessWidget {
  static const String route = '/';
  const HomePage({Key key}) : super(key: key);

  void _navigateToDetailPage(BuildContext context) => Navigator.of(context).pushNamed(DetailPage.route);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Pokedex'),
      ),
      floatingActionButton: FloatingActionButton(onPressed: () {
        _navigateToDetailPage(context);
      }),
    );
  }
}
