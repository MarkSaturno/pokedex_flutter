import 'package:ffuf_flutter_architecture/ffuf_flutter_architecture.dart';
import 'package:flutter/cupertino.dart';
import 'package:pokedex/pokedex.dart';
import 'package:pokedex/state/app_state.dart';

void appStarter() async {
  final persistor = StatePersistor<AppState>(
    StandardEngine(),
    AppStateSerializer(),
  );

  WidgetsFlutterBinding.ensureInitialized();

  AppState initialState;
  try {
    initialState = await persistor.readState();
  } catch (e) {
    print(e);
  }

  final store = Store<AppState>(
    initialState: initialState ?? AppState(),
    actionObservers: [Log.printer(formatter: Log.verySimpleFormatter)],
    persistor: persistor,
  );

  runApp(const MyApp());
}
