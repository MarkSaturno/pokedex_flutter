import 'package:flutter/material.dart';
import 'package:pokedex/feature/details/detail_page_screen.dart';
import 'package:pokedex/feature/homescreen/home_page_screen.dart';

class AppRouter {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case HomePage.route:
        return MaterialPageRoute(
          builder: (_) => const HomePage(),
        );

      case DetailPage.route:
        return MaterialPageRoute(
          builder: (_) => DetailPage(),
        );

      default:
        return MaterialPageRoute(
          builder: (_) => Scaffold(
            body: Center(
              child: Text('Error: No route defined for ${settings.name}'),
            ),
          ),
        );
    }
  }
}
